package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/wacana/beanie/pkg/http/rest"
	"gitlab.com/wacana/beanie/pkg/listing"
	"gitlab.com/wacana/beanie/pkg/storage/mongodb"
)

func main() {
	s, err := mongodb.NewStorage("")
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

	l := listing.NewService(s)
	r := rest.Handler(l)

	fmt.Printf("Starting beanie at http://localhost:%s/\n", os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), r))
}

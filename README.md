# Beanie

<p align="center"><img src="https://vignette.wikia.nocookie.net/sealonline/images/9/9b/Beanie.gif/revision/latest"></img></p>
<p align="center">
    <a href="https://gitlab.com/wacana/beanie/commits/master"><img alt="pipeline status" src="https://gitlab.com/wacana/beanie/badges/master/pipeline.svg" /></a>
    <a href="https://gitlab.com/wacana/beanie/commits/master"><img alt="coverage report" src="https://gitlab.com/wacana/beanie/badges/master/coverage.svg" /></a>
</p>

Wacana REST API

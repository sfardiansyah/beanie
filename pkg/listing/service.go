package listing

// Service provides book listing operations.
type Service interface {
	GetBooks() ([]Book, error)
}

type service struct {
	r Repository
}

// NewService creates a listing service with the necessary dependencies.
func NewService(r Repository) Service {
	return &service{r}
}

func (s *service) GetBooks() ([]Book, error) {
	books, err := s.r.GetAllBooks()
	return books, err
}

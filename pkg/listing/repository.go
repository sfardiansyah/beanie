package listing

// Repository provides access to the books storage
type Repository interface {
	GetAllBooks() ([]Book, error)
}

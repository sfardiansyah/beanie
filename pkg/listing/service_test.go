package listing

import (
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
)

func ServiceSuite(t *testing.T, impl Service) {
	expected := []Book{}
	res, _ := impl.GetBooks()

	if !reflect.DeepEqual(res, expected) {
		t.Errorf("invalid return value: got %+v want %+v", res, expected)
	}
}

func TestServiceWithRepository(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := NewMockRepository(ctrl)

	m.EXPECT().GetAllBooks().Return([]Book{}, nil).Times(1)

	one := NewService(m)
	ServiceSuite(t, one)
}

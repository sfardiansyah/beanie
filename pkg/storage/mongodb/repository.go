package mongodb

import (
	"context"
	"os"
	"time"

	"gitlab.com/wacana/beanie/pkg/listing"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Storage stores data in MongoDB
type Storage struct {
	db *mongo.Client
}

// NewStorage returns new MongoDB storage
func NewStorage(url string) (*Storage, error) {
	var err error

	if url == "" {
		url = os.Getenv("MONGODB_URL")
	}

	s := new(Storage)

	s.db, err = mongo.NewClient(options.Client().ApplyURI(url))
	if err != nil {
		return nil, err
	}

	return s, nil
}

// GetAllBooks retrieves all books in the repository
func (s *Storage) GetAllBooks() ([]listing.Book, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err := s.db.Connect(ctx)

	collection := s.db.Database(os.Getenv("MONGODB_DB")).Collection("books")

	cur, err := collection.Find(ctx, bson.D{})
	defer cur.Close(ctx)

	books := make([]listing.Book, 0)
	err = cur.All(ctx, &books)

	if err != nil {
		return nil, err
	}

	return books, nil
}

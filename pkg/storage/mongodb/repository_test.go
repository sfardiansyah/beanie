package mongodb

import (
	"context"
	"reflect"
	"testing"
	"time"

	"gitlab.com/wacana/beanie/pkg/listing"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func TestRepositoryConnection(t *testing.T) {
	s, err := NewStorage("")

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = s.db.Connect(ctx)
	defer s.db.Disconnect(ctx)

	ctx, _ = context.WithTimeout(context.Background(), 2*time.Second)
	err = s.db.Ping(ctx, readpref.Primary())

	if err != nil {
		t.Errorf(err.Error())
	}

	s.db.Disconnect(ctx)
}

func TestFailedRepositoryConnection(t *testing.T) {
	expected := "error parsing uri: scheme must be \"mongodb\" or \"mongodb+srv\""

	_, err := NewStorage("random")

	if err.Error() != expected {
		t.Errorf("Invalid error raised: want %+v got %+v", err.Error(), expected)
	}
}

func TestGetAllBooks(t *testing.T) {
	expected := []listing.Book{}
	s, err := NewStorage("")

	books, err := s.GetAllBooks()

	if err != nil {
		t.Errorf(err.Error())
	}

	if reflect.TypeOf(books) != reflect.TypeOf(expected) {
		t.Errorf("invalid return value: got %+v want %+v", books, expected)
	}
}

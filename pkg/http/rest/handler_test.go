package rest

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/wacana/beanie/pkg/listing"
)

func createMockListingRepository(ctrl *gomock.Controller) *listing.MockRepository {
	m := listing.NewMockRepository(ctrl)

	m.EXPECT().GetAllBooks().Return([]listing.Book{}, nil).AnyTimes()

	return m
}

func requestGETHandler(t *testing.T, url, expected string, s int, h func(w http.ResponseWriter, r *http.Request)) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	l := listing.NewService(createMockListingRepository(ctrl))

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	handler := Handler(l)
	if h != nil {
		handler = http.HandlerFunc(h)
	}

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, s)
	}

	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestHandler(t *testing.T) {
	requestGETHandler(t, "/", "Hello from Wacana!", http.StatusOK, nil)
}

func TestHomeHandler(t *testing.T) {
	requestGETHandler(t, "/", "Hello from Wacana!", http.StatusOK, homeHandler())
}

func TestHandleBooks(t *testing.T) {
	requestGETHandler(t, "/api/v1/books", "[]", http.StatusOK, nil)
}

func TestGETBooksHandler(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	l := listing.NewService(createMockListingRepository(ctrl))

	requestGETHandler(t, "/api/v1/books", "[]", http.StatusOK, booksHandler(l))
}

package rest

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/wacana/beanie/pkg/listing"
)

// Handler is a function to handle all HTTP Request
func Handler(l listing.Service) http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/", homeHandler())

	s := r.PathPrefix("/api/v1").Subrouter()
	s.HandleFunc("/books", booksHandler(l))

	return r
}

func homeHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello from Wacana!"))
	}
}

func booksHandler(l listing.Service) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		books, _ := l.GetBooks()
		b, _ := json.Marshal(books)

		w.Write(b)
	}
}
